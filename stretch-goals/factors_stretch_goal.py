def factors(number):
    factor_list = []
    for i in range(2, number // 2 + 1):
        if number % i == 0:
            factor_list.append(i)
    return factor_list if factor_list else f'{number} is a prime number'


print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
