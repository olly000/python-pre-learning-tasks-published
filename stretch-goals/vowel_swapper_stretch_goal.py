import re

patterns = [re.compile("[aA]"), re.compile("[eE]"), re.compile("[iI]"), re.compile("[uU]"), re.compile("[oO]")]

swaps = {
        'a': '4',
        'A': '4',
        'e': '3',
        'E': '3',
        'i': '!',
        'I': '!',
        'o': 'ooo',
        'O': '000',
        'u': '|_|',
        'U': '|_|'
    }


def vowel_swapper(string, replacements, patterns):
    listed = list(string)
    for pattern in patterns:
        count = 0
        for m in pattern.finditer(string):
            if m.group() and count == 1:
                index = m.span()[0]
                listed[index] = replacements[string[index]]
            count += 1
    return ''.join(listed)


print(vowel_swapper("aAa eEe iIi oOo uUu", swaps, patterns))  # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World", swaps, patterns))  # Should print "Hello Wooorld" to the console
print(vowel_swapper("Everything's Available", swaps, patterns))  # Should print "Ev3rything's Av/\!lable" to the console